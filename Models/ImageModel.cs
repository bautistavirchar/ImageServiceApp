using Microsoft.AspNetCore.Http;

namespace Models
{
    public class ImageModel
    {
        public string OriginPath { get; set; }
        public IFormFile File { get; set; }
    }

    public class AwsS3Object {
        public IFormFile File { get; set; }
        public string Bucket { get; set; }
        public string Key { get; set; }
    }
}