using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.Extensions.DependencyInjection;

namespace Extensions
{
    public static class DeploymentRegistrars
    {
        public static void DeploymentRegistrar(this IServiceCollection services){
            services.Configure<ForwardedHeadersOptions>(options =>
            {
                options.ForwardedHeaders = 
                    ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto;
                    options.KnownNetworks.Clear();
                    options.KnownProxies.Clear();
            });

            services.AddHttpsRedirection(options =>
            {
                options.RedirectStatusCode = StatusCodes.Status308PermanentRedirect;//StatusCodes.Status307TemporaryRedirect;
                options.HttpsPort = 443;
            });

            services.AddAntiforgery(o => {
                o.SuppressXFrameOptionsHeader = true;
            });
        }
    }
}