using SixLabors.ImageSharp;
using SixLabors.ImageSharp.Formats;
using SixLabors.ImageSharp.Formats.Jpeg;
using SixLabors.ImageSharp.Processing;

namespace Extensions
{
    public class ImageSharpHelper
    {
        public static void Resize(string filePath, string outFilename, int new_Width){
            using (var image = Image.Load(filePath)){
                double w = image.Width;
                double h = image.Height;
                double relation_heigth_width = h / w;
                int new_Height = (int)(new_Width * relation_heigth_width);
                image.Mutate(x => x.Resize(new_Width, new_Height));
                var encoder = new JpegEncoder{
                    Quality = 85
                };
                image.Save(outFilename, encoder); // Automatic encoder selected based on extension.
            }
        }
        public static void Resize(System.IO.FileStream stream, int new_Width){
            IImageFormat format;
            using (var image = Image.Load(stream, out format)){
                double w = image.Width;
                double h = image.Height;
                double relation_heigth_width = h / w;
                int new_Height = (int)(new_Width * relation_heigth_width);
                image.Mutate(x => x.Resize(new_Width, new_Height));
                var encoder = new JpegEncoder{
                    Quality = 85
                };
                image.Save(stream, format); // Automatic encoder selected based on extension.
            }
        }
    }
}