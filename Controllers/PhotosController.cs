using System.Threading.Tasks;
using Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Models;

namespace Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class PhotosController : ControllerBase
    {
        private IEnvironmentDirectory _envService;
        public PhotosController(IEnvironmentDirectory envService) => _envService = envService;

        [HttpPost, Route(nameof(Upload)), DisableRequestSizeLimit]
        public async Task<IActionResult> Upload(string source){
            var file = Request.Form.Files[0];
            // var source = Request.Headers["source"];
            if(file == null || file.Length <= 0 || string.IsNullOrEmpty(source)/*source.Count == 0*/) return BadRequest();
            
            var result = await _envService.SaveImage(new ImageModel {
                File = file,
                OriginPath = source
            });
            return Ok(result);
        }

        [HttpPost, Route(nameof(UploadAws)), DisableRequestSizeLimit]
        public async Task<IActionResult> UploadAws([FromForm]AwsS3Object awsS3Object){
            await _envService.SaveImage(awsS3Object);
            return Ok();
        }

        [HttpGet("{bucket}/{key}")]
        public async Task<IActionResult> Get([FromRoute]string bucket, [FromRoute]string key){
            var file = System.IO.Path.Combine("Resources",bucket,key);
            if(System.IO.File.Exists(file)) {
                var bytes = await System.IO.File.ReadAllBytesAsync(file);
                return File(bytes,"image/jpeg");
            }
            return NotFound();
        }

        [HttpPost,Route(nameof(DeleteObject))]
        public OkObjectResult DeleteObject([FromQuery]string bucket, [FromQuery]string key){
            _envService.Delete(new AwsS3Object{
                Bucket = bucket,
                Key = key
            });
            return Ok(new{ bucket, key });
        }

        [HttpPost,Route(nameof(Delete))]
        public OkResult Delete([FromQuery]string source, string name, string filename){
            _envService.Delete(source,name,filename);
            return Ok();
        }
    }
}