using System.Threading.Tasks;
using Models;

namespace Interfaces
{
    public interface IEnvironmentDirectory
    {
        Task<object> SaveImage(ImageModel image);
        void Delete(string source, string name, string filename);

        
        Task SaveImage(AwsS3Object awsS3Object);
        void Delete(AwsS3Object awsS3Object);
    }
}