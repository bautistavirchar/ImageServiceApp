using System;
using System.Threading.Tasks;
using Extensions;
using Interfaces;
using Models;

namespace Services
{
    public class EnvironmentDirectoryService : IEnvironmentDirectory
    {
        private string RESOURCE_IMAGES_PATH => System.IO.Path.Combine("Resources","images");

        public void Delete(string source, string name, string filename)
        {
            string resource = System.IO.Path.Combine(RESOURCE_IMAGES_PATH,source),
                    path_small = System.IO.Path.Combine(resource,"small"),
                    path_medium = System.IO.Path.Combine(resource,"medium");

            System.IO.File.Delete(System.IO.Path.Combine(resource, filename));
            System.IO.File.Delete(System.IO.Path.Combine(path_small, string.Concat(name, ".jpg")));
            System.IO.File.Delete(System.IO.Path.Combine(path_medium, string.Concat(name, ".jpg")));
        }
        public async Task<object> SaveImage(ImageModel image)
        {
            // set new filename
            var datefile = $"{ Guid.NewGuid() }_{ DateTime.Now.ToFileTime() }";
            string fileType = System.IO.Path.GetExtension (image.File.FileName), // get file extension
                    fileName = string.Concat(datefile, fileType), // set new filename,
                    resource = System.IO.Path.Combine(RESOURCE_IMAGES_PATH,image.OriginPath),
                    fullPath = System.IO.Path.Combine(resource, fileName); // set origin file path
            
            // save
            using (var stream = new System.IO.FileStream(fullPath, System.IO.FileMode.Create)){
                await image.File.CopyToAsync(stream);
            }

            // Thumbnails set to .jpg
            string path_small = System.IO.Path.Combine(resource,"small"),
                    path_medium = System.IO.Path.Combine(resource,"medium"),

                    small_file = System.IO.Path.Combine(path_small, string.Concat (datefile, ".jpg")),
                    medium_file = System.IO.Path.Combine(path_medium, string.Concat (datefile, ".jpg"));

            if(!System.IO.Directory.Exists(path_small)) System.IO.Directory.CreateDirectory(path_small);
            if(!System.IO.Directory.Exists(path_medium)) System.IO.Directory.CreateDirectory(path_medium);

            ImageSharpHelper.Resize(fullPath,small_file,120);
            ImageSharpHelper.Resize(fullPath,medium_file,240);

            return new {
                success = true,
                filename = datefile,
                type = fileType
            };
        }

        public void Delete(AwsS3Object awsS3Object)
        {
            string resource = System.IO.Path.Combine("Resources",awsS3Object.Bucket),
                    file = System.IO.Path.Combine(resource, awsS3Object.Key);
            
            if(!System.IO.Directory.Exists(resource) || !System.IO.File.Exists(file)) return;

            System.IO.File.Delete(file);
        }

        public async Task SaveImage(AwsS3Object awsS3Object)
        {
            string resource = System.IO.Path.Combine("Resources",awsS3Object.Bucket),
                    fullPath = System.IO.Path.Combine(resource, awsS3Object.Key); // set origin file path
            
            if(!System.IO.Directory.Exists(resource)){
                System.IO.Directory.CreateDirectory(resource);
            }

            // save
            using (var stream = new System.IO.FileStream(fullPath, System.IO.FileMode.Create)){
                await awsS3Object.File.CopyToAsync(stream);
            }
        }

        // ..
    }
}