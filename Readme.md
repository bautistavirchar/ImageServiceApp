## Image uploader REST API .NET Core 3.1
---

```
http://your-domain.com/photos/uploadaws
```
> Parameters
> * **bucket**
>   * type: ```FromForm```
>   * folder name
> * **key**
>   * type: ```FromForm```
>   * filename
> * **file**
>   * type: ```multiform-data```

```
http://your-domain.com/photos/deleteobject?bucket=rental&key=sample.jpg
```
> Parameters
> * **bucket**
>   * type: ```FromQuery```
>   * folder name
> * **key**
>   * type: ```FromQuery```
>   * filename to delete

```
http://your-domain.com/folder-name/sample.jpg
```
> Parameters
> * **folder-name**
>   * type: ```route```
>   * folder name
> * **sample.jpg**
>   * type: ```route```
>   * filename to view

```
http://localhost:5003/photos/upload
```
> Parameters
> * **source**
>   * type: ```header```
>   * folder path
> * **file**
>   * type: ```multiform-data```

```
http://localhost:5003/photos/delete
```
> Parameters
> * source
>   * type: ```folder path```
> * name
>   * type: ```name of file without file extension```
> * filename
>   * type: ```file name with file extension```
>   * Sample
>   ```http://localhost:5003/photos/delete?source=domain.com&name=my-image-name&filename=my-image-name.jpg```

```
http://image-viewer.com/
```
> Add file
> * ```web.config```
> * code below...

```xml
    <?xml version="1.0" encoding="utf-8"?>
    <configuration>
    <location path="." inheritInChildApplications="false">
        <system.webServer>
        <httpProtocol>
            <customHeaders>
            <add name="Access-Control-Allow-Origin" value="*" />
            <add name="Access-Control-Allow-Headers" value="Content-Type" />
            <add name="Access-Control-Allow-Methods" value="GET" />
            </customHeaders>
        </httpProtocol>
        </system.webServer>
    </location>
    </configuration>
```